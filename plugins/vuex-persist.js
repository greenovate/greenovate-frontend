import VuexPersistence from 'vuex-persist/dist/umd'

export default ({ store }) => {
  return new VuexPersistence({
    storage: window.localStorage,
    reducer: state => ({ isNeubau: state.isNeubau })
  }).plugin(store)
}
