const fs = require('fs')

const routesPromise = () => {
  const pageRoutes = fs.readdirSync('./assets/content/pages').map(file => ({
    route: `/${file.slice(0, -5)}`,
    payload: require(`./assets/content/pages/${file}`)
  }))
  const modules = fs.readdirSync('./assets/content/leitfaden')
  const moduleRoutes = []
  const submoduleRoutes = []
  for (const mod of modules) {
    const moduleData = require(`./assets/content/leitfaden/${mod}`)
    moduleRoutes.push({
      route: `/leitfaden/${mod.slice(0, -5)}`,
      payload: {
        title: moduleData.title,
        content: moduleData.submodules.bestand
          ? {
            bestand: moduleData.submodules.bestand,
            neubau: moduleData.submodules.bestand
          }
          : moduleData.content,
        submodules: Object.keys(moduleData.submodules)
      }
    })
    for (const submodule of moduleRoutes[moduleRoutes.length - 1].payload.submodules) {
      submoduleRoutes.push({
        route: `/leitfaden/${mod.slice(0, -5)}/${submodule}`,
        payload: {
          content: moduleData.submodules[submodule]
        }
      })
    }
  }
  return [...pageRoutes, ...moduleRoutes, ...submoduleRoutes]
}

routesPromise()
