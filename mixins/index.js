import { structure } from '~/assets/content/meta.json'

export const activeModule = {
  computed: {
    activeModule () {
      return this.$route.params.module
        ? structure.filter(mod => mod.slug === this.$route.params.module)[0]
        : null
    }
  }
}

export const activeSubModule = {
  computed: {
    activeSubModule () {
      return this.$route.params.submodule && this.activeModule
        ? this.activeModule.subModules.filter(mod => mod.slug === this.$route.params.submodule)[0]
        : null
    }
  }
}

export const isLandingPage = {
  computed: {
    isLandingpage () {
      return this.$nuxt.$route.path === '/'
    }
  }
}
