import GNavBar from '~/components/navigation/GNavBar.vue'
import GNavGlobal from '~/components/navigation/GNavGlobal.vue'
import GNavGlobalMenu from '~/components/navigation/GNavGlobalMenu'
import GNavSite from '~/components/navigation/GNavSite.vue'

export {
  GNavBar,
  GNavGlobal,
  GNavGlobalMenu,
  GNavSite
}
