import InfoSlider from '~/components/infografik/InfoSlider.vue'
import InfoImageView from '~/components/infografik/InfoImageView.vue'
import InfoLegend from '~/components/infografik/InfoLegend.vue'
import InfoHeatDistribution from '~/components/infografik/InfoHeatDistribution.vue'
import InfoRating from '~/components/infografik/InfoRating'
import InfoText from '~/components/infografik/InfoText'

export {
  InfoSlider,
  InfoImageView,
  InfoLegend,
  InfoHeatDistribution,
  InfoRating,
  InfoText
}
