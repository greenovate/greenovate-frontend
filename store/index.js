export const state = () => ({
  isNeubau: true,
  showNav: false
})

export const actions = {
  setNeubau ({ commit }, newValue) {
    commit('setNeubau', newValue)
  },
  toggleShowNav ({ commit, state }) {
    commit('setShowNav', !state.showNav)
  }
}

export const mutations = {
  setNeubau (state, newValue) {
    state.isNeubau = newValue
  },
  setShowNav (state, newValue) {
    state.showNav = newValue
  }
}

export const getters = {
  getNeubau: state => state.isNeubau,
  getShowNav: state => state.showNav
}
