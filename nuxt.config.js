import fs from 'fs'
import webpack from 'webpack'
import meta from './assets/content/meta.json'

const routesPromise = new Promise(resolve => {
  const pageRoutes = fs.readdirSync('./assets/content/pages').map(file => ({
    route: `/${file.slice(0, -5)}`,
    payload: require(`./assets/content/pages/${file}`)
  }))
  const modules = fs.readdirSync('./assets/content/leitfaden')
  const moduleRoutes = []
  const submoduleRoutes = []
  for (const mod of modules) {
    const moduleData = require(`./assets/content/leitfaden/${mod}`)
    moduleRoutes.push({
      route: `/leitfaden/${mod.slice(0, -5)}`,
      payload: {
        title: moduleData.title,
        content: !moduleData.content
          ? {
            bestand: moduleData.bestand,
            neubau: moduleData.neubau
          }
          : moduleData.content,
        submodules: Object.keys(moduleData.submodules)
      }
    })
    for (const submodule of moduleRoutes[moduleRoutes.length - 1].payload.submodules) {
      submoduleRoutes.push({
        route: `/leitfaden/${mod.slice(0, -5)}/${submodule}`,
        payload: {
          content: moduleData.submodules[submodule]
        }
      })
    }
  }
  resolve([...pageRoutes, ...moduleRoutes, ...submoduleRoutes])
})

export default {
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'de'
    },
    title: 'Greenovate K(r)EMs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: meta.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/global.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/vuex-persist.js', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/markdownit',
    '@nuxtjs/sitemap'
  ],
  markdownit: {
    html: true,
    injected: true,
    quotes: '„“‚‘'
  },
  sitemap: {
    hostname: 'http://www.greenovate.at',
    routes () {
      return routesPromise
    }
  },
  /*
  ** Build configuration
  */
  build: {
    babel: {
      babelrc: true,
      presets ({ isServer }) {
        const targets = isServer ? { node: '10' } : { ie: '11' }
        return [
          [require.resolve('@nuxt/babel-preset-app'), { targets }]
        ]
      }
    },
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        'postcss-for': {},
        'postcss-mixins': {},
        'postcss-simple-vars': {},
        'postcss-color-mod-function': {},
        'postcss-url': false,
        'postcss-nested': {},
        'postcss-calc': {}
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: true
        }
      }
    },
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /moment$/
      })
    ],
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.devtool = 'eval-source-map'
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    routes () {
      return routesPromise
    }
  }
}
